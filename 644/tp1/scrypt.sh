#!/usr/bin/sh

i=0
arg=""
for var in $@
do
    if [ $i -gt 2 ]
    then
	arg="$arg $var"
    fi
    i+=1
done
$2 $arg > $1 2>&1

