#!/usr/bin/sh

i=0
a=2

if [ $# -lt 2 ]
then
    echo "./script.sh [FILE] [-d] [COMMAND] [PARAMS]"
    exit 2
fi


if [ $2 = '-d' ]
then
    a=3
fi

arg=""
for var in $@
do
    if [ $i -gt $a ]
    then
	echo $var
	arg="$arg $var"
    fi
    i+=1
done

echo $2
echo $arg

if [ $2 = '-d' ]
then
    date > $1
    echo $arg
    $3 $arg >> $1 2>&1
else
    $2 $arg >> $1 2>&1
fi

#echo "toto = " $$a
#$"$a" $arg #>> $1 2>&1

