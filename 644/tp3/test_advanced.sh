#!/usr/bin/sh

rm test1-0 test1-1 /tmp/test test2-0 /tmp/test2-1
echo "test1" > test1-0
echo "test1" > test1-1
echo "test1" > /tmp/test
echo "Creating file test1-0 in current directory"
echo "Creating file test1-1 in current directory"
echo "Creating file test in /tmp"
echo "test1-0 is the same as test1-1"

echo "Same" > test2-0
echo "Same" > /tmp/test2-1


echo "./advanced.sh test1-0 test1-1 /tmp/test test2-0 /tmp/test2-1" 
