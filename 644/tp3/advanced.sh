#!/usr/bin/sh

USAGE='USAGE: $0 [FILE1] [FILE2] [...]'
idx=0
arr=$@

test $# -lt 2 && echo $USAGE && exit 1
for a in $arr
do
    filesystem=''
    list=''
    right=''
    size=''
    owner=''
    groups=''
    test -f $a &&
	filesystem=$(df -- $a | tail -n1 | tr ' ' '\t' | cut -f1) &&
	list=$(ls -l -- $a  | tr -s ' ' '\t') &&
	right=$(ls -l -- $a | tr -s ' ' '\t' | cut -f1) &&
	size=$(ls -l  -- $a  | tr -s ' ' '\t' | cut -f5) &&
	owner=$(ls -l -- $a | tr -s ' ' '\t' | cut -f3) &&
	groups=$(ls -l -- $a | tr -s ' ' '\t' | cut -f4) &&
	inode=$(ls -i -- $a | tr -s ' ' '\t' | cut -f1)
    str=$(echo "$filesystem;$right;$size;$owner;$groups")
    store[$idx]=$str
    idx=$((idx+1))
    
done

idx=0
for a in $@
do
    arr[$idx]=$a
    idx=$((idx+1))
done

idx=0
while test $idx -lt $#
do
    str=$(echo ${store[$idx]})
    ! test -f ${arr[$idx]} && idx=$((idx+1)) && continue
    jdx=0
    while test $jdx -lt $#
    do
	! test -f ${arr[$jdx]} && jdx=$((jdx+1)) && continue
	test $idx -eq $jdx && jdx=$((jdx+1)) && continue
	st=$(echo ${store[$jdx]})
	test $str != $st && jdx=$((jdx+1)) && continue
	inode=$(ls -li ${arr[$idx]} | tr ' ' '\t' | cut -f1)
	inod=$(ls -li ${arr[$jdx]} | tr ' ' '\t' | cut -f1)
	test $inode -eq $inod && jdx=$((jdx+1)) && continue
	mdone=$(md5sum ${arr[$idx]} | tr ' ' '\t' | cut -f1)
	mdtwo=$(md5sum ${arr[$idx]} | tr ' ' '\t' | cut -f1)
	test $mdone != $mdtwo && jdx=$((jdx+&)) && continue
	echo "Creating hardlink between ${arr[$idx]} ${arr[$jdx]}"
	ln -f ${arr[$idx]} ${arr[$jdx]} && echo "Hardlink created"
	
	jdx=$((jdx+1))
    done
    idx=$((idx+1))
done
