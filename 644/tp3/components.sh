#!/usr/bin/sh

USAGE='./component.sh [FILE_NAME]'
test $# != 1 && echo $USAGE && exit 1
readlink -f $1 | tr -s  '/' '\n' | sed 1d
