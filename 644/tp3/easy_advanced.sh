#!/usr/bin/sh

USAGE='USAAGE: $0 [FILE1] [FILE2]'
test $# -lt 2 && echo $USAGE && exit 1

function compare_inode()
{
    local first=$(stat -c "%i" $1)
    local sec=$(stat -c "%i" $2)
    test $first -ne $sec && return 0
    return 1
}

function compare_file_system()
{
    local first=$(stat -c "%d" $1)
    local sec=$(stat -c "%d" $2)
    test $first -eq $sec && return 0
    return 1
}

function compare_md5()
{
    local first=$(md5sum $1 | tr ' ' '\t' | cut -f1) 
    local sec=$(md5sum $2 | tr ' ' '\t' | cut -f1)
    test $first = $sec && return 0
    return 1
}

function compare_size()
{
    local first=$(stat -c "%s" $1)
    local sec=$(stat -c "%s" $2)
    test $first -eq $sec && return 0
    return 1
}

function compare_owner()
{
    local first=$(stat -c "%u" $1)
    local sec=$(stat -c "%u" $2)
    test $first -eq $sec && return 0
    return 1
}

function compare_group()
{
    local first=$(stat -c "%g" $1)
    local sec=$(stat -c "%g" $2)
    test $first -eq $sec && return 0
    return 1
}

function compare_perm()
{
    local first=$(stat -c "%A" $1)
    local sec=$(stat -c "%A" $2)
    test $first = $sec && return 0
    return 1
}

while test $# -ge 2
do
    compare_size $1 $2 &&
    compare_owner $1 $2 &&
    compare_group $1 $2 &&
    compare_perm $1 $2 &&
    compare_inode $1 $2 &&
    compare_file_system $1 $2 &&
    compare_md5 $1 $2 &&
    ln -f $1 $2 && echo "Hardlink has been created between $1 (source) and $2 (dest)"
    shift 2
done
