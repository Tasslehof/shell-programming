#!/usr/bin/sh

nb=10
test $# -ne 0 && nb=$1
test $nb -lt 0 && nb=$(($nb*-1))
test $nb -eq 0 && echo "USAGE: $0 [NUMBER]" && exit 1
sed -e "1,$nb! d"
