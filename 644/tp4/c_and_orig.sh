#!/usr/bin/sh

dir=./sys/bin/sh/

find $dir*.c | sed -e "s/.*/&\n&.orig/" | xargs -n 2 diff -q

