#!/usr/bin/sh

echo "12 123 45 6" | sed -re "s/\b([0-9])([0-9])\b/\2\1/g;n"
echo "le salut 42 fois mais le aurevoir 24 fois" | sed -re "s/\b([1-9])([0-9])\b/\2\1/g;n"
