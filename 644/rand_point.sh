#!/usr/bin/sh

USAGE="USAGE: $0 [LOW] [MAX] [NB_POINT]"

test $# -ne 3 && echo $USAGE && exit 1
test $1 -gt $2 && echo $USAGE && exit 1
ABS=$(shuf -i $1-$2 -n $3)
ORD=$(shuf -i $1-$2 -n $3)
IDX=1

while test $IDX -le $3
do
    echo '('$(echo $ABS | tr ' ' '\t' | cut -f$IDX)','$(echo $ORD | tr ' ' '\t' | cut -f$IDX)')'
    IDX=$(($IDX+1))
done


