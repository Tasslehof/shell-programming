#!/bin/sh

PREFIX="_capture_kashyyyk"
test -z $TMPDIR && TMPDIR=/tmp
test $# -eq 0 && find $TMPDIR/$PREFIX -type f
test $# -gt 0 && cmd=$1 && file=$(mktemp -t) && shift && $cmd $@ > $file && echo $file
