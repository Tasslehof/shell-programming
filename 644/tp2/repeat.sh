#!/usr/bin/sh

function repeat()
{
    cmd=$1
    shift
    while true
    do
	$cmd $@ &
	sleep 1
    done
}

repeat $@
