#!/usr/bin/sh

function usable()
{
    command which $@ >&2-
}

usable $@
