#!/usr/bin/sh

USAGE="usage: $0 START STOP"

function seq_func() 
{
    local first=$1
    local last=$2
    local infloop=$3
    while test $first -le $last || $infloop == true
    do
	echo $first
	first=$(($first+1))
    done
    return 0
}

test $# -eq 1 && seq_func 1 $1 false
test $# -eq 2 && seq_func $1 $2 false && shift
test $# -gt 1 && echo $USAGE && exit 1
test $# == 0 && seq_func 1 0 true
exit 0

    
